# Guia Foundation + Compass

*Esta guía explica la creación de un nuevo proyecto en Foundation en GNU/Linux.*

Tenemos que tener instalado todo esto:

* NodeJS
* Ruby 1.9
* Bower
* Compass
* Foundation

La instalación de estos paquetes es relativamente sencilla. En el caso de NodeJS será necesario dar de alta un repo extra. Tanto Compass y Foundation se instalan a través de **gem**.

#### Creación de proyecto

```bash
$ foundation new mi_proyecto
```

Este comando creará un nuevo proyecto Foundation en la carpeta "mi_proyecto".

#### Estructura de proyecto

Los elementos a tener en cuenta son:

* config.rb - Archivo de configuración utilizado por Compass
* scss/ - Aquí residen los archivos SCSS que vayamos agregando
* js/ - Lo mismo pero para Javascript
* css/ - En esta carpeta se guardan los CSS que vayamos compilando con Compass
* bower_components/ - Aquí se almacenan los paquetes instalados a través de Bower

#### Configuración

Es necesario editar el archivo de configuración *config.rb* para que utilice las carpetas creadas por foundation.

```
# Require any additional compass plugins here.
add_import_path "bower_components/foundation/scss"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "img"
javascripts_dir = "js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
```

#### Plantilla original

La plantilla *index.html* debe ser modificada para tener las rutas correctas:

```html
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Foundation 5</title>

  <!-- If you are using CSS version, only link these 2 files, you may add app.css to use for your overrides if you like. -->
  <link rel="stylesheet" href="bower_components/foundation/css/normalize.css">
  <link rel="stylesheet" href="bower_components/foundation/css/foundation.css">

  <!-- If you are using the gem version, you need this only -->
  <link rel="stylesheet" href="css/app.css">

  <script src="bower_components/modernizr/modernizr.js"></script>

</head>
<body>

  <!-- body content here -->

  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="bower_components/foundation/js/foundation.min.js"></script>
  <script>
    $(document).foundation();
  </script>
</body>
</html>
```
#### Activando compass

El comando *compass watch* nos permite generar el css de nuestra aplicación en cada modificación que hagamos a los archivos en la carpeta scss.
Este comando queda corriendo por lo que es necesario levantarlo por cada cambio realizado.

```bash
$ compass watch
```

#### Agregar dependecias

Bower nos permite agregar elementos a nuestra aplicación especificando el nombre del paquete en el archivo bower.js o directamente desde linea de comando.
En caso de hacer lo segundo podemos definir que la dependencia se agregue al archivo de dependencias automaticamente agregando *--save*.

```bash
$ bower install font-awesome --save
```